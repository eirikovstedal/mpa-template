const PR_VAR = 'MPA_PAGEREFRESH_LOCALE_STORAGE_VARIABLES#';

function handleFocus() {
  const delay = localStorage.getItem(PR_VAR + 'delay');
  setTimeout(async () => {
    const resp = await fetch('/updated.txt');
    const [time, delayTxt] = (await resp.text()).split(' ');
    const lastTime = localStorage.getItem(PR_VAR + 'lastTime');
    if (time === lastTime) return;
    localStorage.setItem(PR_VAR + 'delay', delayTxt);
    localStorage.setItem(PR_VAR + 'lastTime', time);
    location.reload();
  }, parseInt(delay) > 0 ? parseInt(delay) + 400 : 1000);
}

window.addEventListener('focus', handleFocus);