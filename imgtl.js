const { fork } = require('child_process');

async function _srcset({ path, file, output }) {
  return new Promise(resolve => {
    const child = fork('compress');
    child.on('message', message => resolve(message))
    child.send([path, file, output]);
  })
}
exports.srcset = _srcset;