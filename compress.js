const fs = require('fs');
const sharp = require('sharp');
const config = require('./config').default;

const quality = config.img.quality || 35;
const res = config.img.res;
const imgFileFilter = new RegExp(`\\.(${config.img.filter.join('|')})$`, 'i');

async function main(message) {
  const [path, file, output] = message;
  const fullPath = path + '/' + file;
  const s_file = sharp(fullPath);
  const metadata = await s_file.metadata();
  const [o_width, o_height] = [metadata.width, metadata.height];
  const srcsetOutput = [];
  const dirPath = output + '/' + file.replace(imgFileFilter, '-$1');
  if (!fs.existsSync(dirPath)) fs.mkdirSync(dirPath, { recursive: true });
  const promises = [];
  for (let width of res) {
    const outputPath = dirPath + '/' + `${width}.webp`;
    srcsetOutput.push({ size: width, src: outputPath });
    if (width > o_width) width = o_width;
    if (!fs.existsSync(outputPath))
      promises.push(s_file.resize({ width }).webp({ quality }).toFile(outputPath));
  };
  await Promise.all(promises);
  process.send({ srcset: srcsetOutput, fullPath, aspectRatio: [res[0], Math.round(res[0] / o_width * o_height)] });
  process.exit();
}

process.on('message', (message) => {
  main(message);
});