# MPA template

This is a simple multi page app template, with semi-live reload. The included build tool optimizes images and convertes markdown files to html files.

## Reason

This is a simple alternative to all the complex tools out there to create websites/apps. It only has a few dev-dependency and should be easy to get started with.

## Things to know

This template builds and serves static pages from a template.
The template should contain your app-shell, with navigation.

The src folder contain all the pages in your website. At build time those are injected into the template recursively. The pages can contain html, styles and javascript.

Folders within the src folder will be copied to the build folder at build time.

## Command line

Serve the page locally with  
`npm run dev`  

And build the app with  
`npm run build`