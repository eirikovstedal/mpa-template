
const config = {
  build: {
    source: 'src',
    output: 'build'
  },
  img: {
    filter: ["jpg", "jpeg", "png", "gif"],
    format: 'webp',
    quality: 35,
    res: [600, 1000, 1600, 2400],
  },
}

exports.default = config;