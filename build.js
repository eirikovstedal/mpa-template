const fs = require('fs');
const marked = require('marked');
const config = require('./config').default;
const imgtl = require('./imgtl');
const os = require('os')
const cpuCount = os.cpus().length;
const createSrcset = imgtl.srcset;
const SOURCE = config.build.source;
const OUTPUT = config.build.output;
const ENV_DEV = process.argv.includes('--dev');
const FILESTRUCTURE = { type: 'DIR', name: SOURCE, files: [] };
const toReplace = [
  { type: 'style', placement: '%STYLE%' },
  { type: 'script', placement: '%SCRIPT%' },
  { type: 'html', placement: '%CONTENT%' }
];
const imageFilter = new RegExp(`\\.(${config.img.filter.join('|')})`, 'i');
const imgFormats = config.img.filter.map(e => e.toLowerCase());

function cfp(path, file, repl) {
  path = (path ? path + '/' : '') + file;
  if (repl) path = path.replace(new RegExp(`^${SOURCE}`), OUTPUT);
  return path;
}

function clearBuild(path) {
  const buildFiles = fs.readdirSync(path);
  for (const file of buildFiles) {
    const cpath = path + '/' + file;
    if (fs.lstatSync(cpath).isDirectory())
      fs.rmSync(cpath, { recursive: true, force: true });
    else fs.unlinkSync(cpath);
  }
}

function createFileType(path, name) {
  if (fs.lstatSync(path + '/' + name).isDirectory())
    return {
      type: 'DIR',
      name,
      files: [],
      path
    }
  return {
    type: 'FILE',
    name,
    fileType: name.substr(name.lastIndexOf('.') + 1).toLowerCase(),
    path
  }
}

function createFolderStructure({ src, fileStruct }) {
  const folders = fs.readdirSync(src);
  for (const entity of folders) {
    let file = createFileType(src, entity);
    if (file.type === 'DIR')
      file = createFolderStructure({
        src: file.path + '/' + file.name,
        fileStruct: file,
      })
    fileStruct.files.push(file);
  }
  return fileStruct;
}

function buildFolderStructure({ fileStruct, path }) {
  path = cfp(path, fileStruct.name, true);
  if (!fs.existsSync(path)) fs.mkdirSync(path);
  for (const folder of fileStruct.files.filter(f => f.type === 'DIR'))
    buildFolderStructure({ fileStruct: folder, path })
}

async function buildImages({ fileStruct, images = {} }) {
  let promises = [];
  for (const file of fileStruct.files) {
    if (file.type === 'DIR') images = await buildImages({ fileStruct: file, images });
    if (imgFormats.includes(file.fileType)) {
      promises.push(createSrcset({
        path: file.path,
        file: file.name,
        output: file.path.replace(new RegExp(`^${SOURCE}`), OUTPUT)
      }));
      if (promises.length >= cpuCount) {
        const all = await Promise.all(promises);
        all.forEach(image => {
          images[image.fullPath] = { srcset: image.srcset, aspectRatio: image.aspectRatio };
        })
        promises = [];
      }
    }
  }
  const all = await Promise.all(promises);
  all.forEach(image => {
    images[image.fullPath] = { srcset: image.srcset, aspectRatio: image.aspectRatio };
  })
  return images;
}

function writeFile(file, str) {
  fs.writeFileSync(cfp(file.path, file.name, true).replace(/(\.md)$/i, '.html'), str);
}

function mergeWithTemplate(html, template) {
  for (const { type, placement } of toReplace) {
    if (type === 'html') {
      template = template.replace(placement, html);
      continue;
    }
    const regex = new RegExp(`<${type}([\\s\\S]+?)<\/${type}.*>`, 'g');
    while (html.match(regex)) {
      const content = html.match(regex);
      html = html.replace(regex, '');
      template = template.replace(placement, content);
    }
    const placementRegex = new RegExp(placement, 'g');
    template = template.replace(placementRegex, '');
  }
  return template;
}

function createImgTag({ img, imageInfo, lazy }) {
  const srcset = imageInfo.srcset
    .map(res => `${res.src.replace(OUTPUT, '')} ${res.size}w`)
    .join(', ');
  let out = img.replace(imageFilter, `-$1/${config.img.res[2]}.${config.img.format}`);
  const lazyLoading = lazy ? 'loading="lazy"' : '';
  out = out.replace(
    '<img',
    `<img srcset="${srcset}" width="${imageInfo.srcset[2].size}" style="aspect-ratio: ${imageInfo.aspectRatio.join('/')}" ${lazyLoading}`
  );
  return out;
}

function replaceImageLinks({ fileStruct, output, images }) {
  const allImg = (output.match(/<img.*?>/g) || []).filter(e => !e.includes('.ico'));
  for (const [i, img] of allImg.entries()) {
    const src = img.match(/(?<=src=("|'|`)).*?(?=("|'|`))/g)?.[0];
    const buildSrc = src.startsWith('/') ? SOURCE + src : fileStruct.path + '/' + src;
    const imageInfo = images[buildSrc];
    if (!imageInfo) continue;
    output = output.replace(img, createImgTag({ img, imageInfo, lazy: i > 1 }));
  }
  return output;
}

function buildHtml({ fileStruct, images, template }) {
  let ftpl = fileStruct.files.find(f => f.name === '__template.html');
  if (ftpl) {
    ftpl = fs.readFileSync(ftpl.path + '/' + ftpl.name, 'utf8');
    if (template !== undefined) ftpl = mergeWithTemplate(ftpl, template);
  }
  template = ftpl || template;
  if (!template) return;
  for (const file of fileStruct.files) {
    if (!['html', 'md'].includes(file.fileType) || file.name === '__template.html') {
      if (file.type === 'DIR') buildHtml({ fileStruct: file, images, template });
      continue;
    };
    let html = fs.readFileSync(file.path + '/' + file.name, 'utf8');
    if (file.fileType === 'md') html = marked.parse(html);
    let output = mergeWithTemplate(html, template);
    output = replaceImageLinks({ fileStruct: file, output, images });
    if (ENV_DEV) output = output.replace('</body>', '<script src="/pagerefresh.js"></script>\n</body>');
    writeFile(file, output);
  }
}

function buildMisc({ fileStruct }) {
  fileStruct.files.forEach(file => {
    if (file.type === 'DIR') return buildMisc({ fileStruct: file });
    if (['html', 'md', ...imgFormats].includes(file.fileType)) return;
    const fullPath = file.path + '/' + file.name;
    fs.copyFileSync(fullPath, fullPath.replace(SOURCE, OUTPUT));
  })
}

async function main() {
  console.time('build time');
  const time = Date.now();
  if (!ENV_DEV) clearBuild(OUTPUT);
  let fileStruct = createFolderStructure({ src: SOURCE, fileStruct: FILESTRUCTURE });
  buildFolderStructure({ fileStruct });
  const images = await buildImages({ fileStruct });
  buildHtml({ fileStruct, images });
  buildMisc({ fileStruct });
  console.timeEnd('build time')
  if (ENV_DEV) {
    if (!fs.existsSync(OUTPUT + '/pagerefresh.js'))
      fs.copyFileSync('pagerefresh.js', OUTPUT + '/pagerefresh.js');
    const now = Date.now();
    fs.writeFileSync(OUTPUT + '/updated.txt', now + ' ' + (now - time));
  }
}

main();