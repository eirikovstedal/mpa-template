const time = Date.now();

const fs = require('fs');
const imgtl = require('./imgtl');
const marked = require('marked');
const config = require('./config').default;

const imageEnd = new RegExp(config.img?.filter?.source + '$', 'i');
const createSrcset = imgtl.srcset;
const getAspectRatio = imgtl.aspratio;
const ENV_DEV = process.argv[2] === '--dev';
const rootReadPath = config.build.source;
const rootBuildPath = config.build.output;
const imgProps = ['filter', 'res', 'format', 'quality'];
const toReplace = [
  { type: 'style', placement: '%STYLE%' },
  { type: 'script', placement: '%SCRIPT%' },
  { type: 'html', placement: '%CONTENT%' }
]

function clearBuild(path) {
  const build_files = fs.readdirSync(path);
  for (const file of build_files) {
    const cpath = path + '/' + file;
    if (fs.lstatSync(cpath).isDirectory())
      fs.rmSync(cpath, { recursive: true, force: true });
    else fs.unlinkSync(cpath);
  }
}

function createImgTag({ img, src, aspectRatio, lazy }) {
  const srcPath = src.replace(config.img.filter, '-$1');
  const srcset = config.img.res
    .map(res => `${srcPath}/${res}.${config.img.format} ${res}w`)
    .join(', ');
  let out = img.replace(config.img.filter, `-$1/${config.img.res[2]}.${config.img.format}`);
  const lazyLoading = lazy ? 'loading="lazy"' : '';
  out = out.replace(
    '<img',
    `<img srcset="${srcset}" width="${config.img.res[2]}" style="aspect-ratio: ${aspectRatio.join('/')}" ${lazyLoading}`
  );
  return out;
}

async function updateImageTags({ template, readPath }) {
  const allImg = (template.match(/<img.*?>/g) || []).filter(e => !e.includes('.ico'));
  for (const [i, img] of allImg.entries()) {
    const src = img.match(/(?<=src=("|'|`)).*?(?=("|'|`))/g)?.[0];
    if (!src || src.endsWith(`.${config.img.format}`) /* Not the way to do it? */) continue;
    const path = src.startsWith('/') ?
      rootReadPath + src :
      readPath + '/' + src;
    const aspectRatio = await getAspectRatio({ path });
    template = template.replace(img, createImgTag({ img, src, aspectRatio, lazy: i > 1 }));
  }
  return template.replace(config.img.filter, `-$1/${config.img.res[2]}.${config.img.format}`);
}

async function replaceInTemplate({ readFile, readPath, file, scrTemplate }) {
  let html = readFile || fs.readFileSync(readPath + '/' + file, 'utf8');
  if (file?.endsWith('.md')) html = marked.parse(html);
  let template = fs.readFileSync(scrTemplate || readPath + '/__template.html', 'utf8');
  toReplace.forEach(({ type, placement }) => {
    if (type === 'html') return (template = template.replace(placement, html));
    var st = new RegExp(`<${type}.*>`, "g");
    var en = new RegExp(`<\/${type}.*>`, "g");
    const startPos = html.search(st);
    const endPos = html.search(en);
    const content = html.slice(startPos, endPos + type.length + 3);
    html = html.replace(content, '');
    template = template.replace(placement, content);
  });
  if (imgProps.every(function(x){return x in config.img}))
    template = await updateImageTags({ template, readPath });
  template = template.replace('</body>', ENV_DEV ? '<script src="/pagerefresh.js"></script>\n</body>' : '</body>');
  
  if (!ENV_DEV) {
    template = template.replace(/^\s*$(?:\r\n?|\n|)/gm, '');
    template = template.replace(/\s+/g, " ");
  }
  console.log(`Building ${file}`);
  return template;
}

async function build(readPath, buildPath) {
  const files = fs.readdirSync(readPath);
  if (!fs.existsSync(buildPath)) fs.mkdirSync(buildPath, { recursive: true });
  const templateExists = fs.existsSync(readPath + '/__template.html');
  for (const file of files) {
    if (file.includes('__template')) continue;
    else if (fs.lstatSync(readPath + '/' + file).isDirectory())
      await build(readPath + '/' + file, buildPath + '/' + file);
    else if (file.match(imageEnd))
      createSrcset({ path: readPath, file, output: buildPath });
    else if (!templateExists || !(file.endsWith('.html') || file.endsWith('.md')))
      fs.copyFileSync(readPath + '/' + file, buildPath + '/' + file);
    else {
      let template = await replaceInTemplate({ readPath, file });
      if (templateExists && readPath.split('/').length > 1)
        template = await replaceInTemplate({ readFile: template, file: '/__template.html', scrTemplate: rootReadPath + '/__template.html' });
      fs.writeFileSync(buildPath + '/' + file.replace(/(\.md)$/i, '.html'), template);
    }
  }
  return true;
}

async function init() {
  if (!ENV_DEV) clearBuild(rootBuildPath);

  await build(rootReadPath, rootBuildPath);

  if (ENV_DEV) {
    if (!fs.existsSync(rootBuildPath + '/pagerefresh.js'))
      fs.copyFileSync('pagerefresh.js', rootBuildPath + '/pagerefresh.js');
    const now = Date.now();
    fs.writeFileSync(rootBuildPath + '/updated.txt', now + ' ' + (now - time));
  }
}

init();