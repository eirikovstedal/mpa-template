const fs = require('fs');
var { exec } = require('child_process');

const srcDir = process.argv[2] || 'src';

const getDirectories = source =>
  fs.readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => source + '/' + dirent.name)

const dirs = getDirectories(srcDir);
dirs.unshift(srcDir);

dirs.forEach(dir => {
  let fsWait;
  fs.watch(dir, (event, filename) => {
    if (filename) {
      if (fsWait) clearTimeout(fsWait);
      fsWait = setTimeout(() => {
        console.log(`File ${filename} changed. Rebuilding..`);
        exec('node build.js --dev')
      }, 50);
    }
  });
})